"Remaps the esc key 'jk'"
inoremap jk <ESC>

"Changes the leader key from default to SPACE"
let mapleader=" "

"Turning on syntax highlighting"
syntax on

"Sets default encoding and sets vim paste buffer to system buffer"
set encoding=utf-8
set clipboard=unamedplus

set tabstop=4
set softtabstop=4
set shiftwidth=4

set smartindent
set smarttab
